// ~ Load Module Dependencies ~ //
var express = require('express');
var router = express.Router();

var itemsRouter = require('./items_r');
var customerRouter = require('./customers_r');
var categoryRouter = require('./categories_r');
var itemPriceRouter = require('./item_prices_r');
var proformaItemsRouter = require('./proforma_items_r');
var proformaRouter = require('./proformas_r');
var saleItemRouter = require('./sale_item_r');
var salesRouter = require('./sales_r');
var stockItemsRouter = require('./stock_items_r');
var stockRouter = require('./stock_r');
var subCategoriesRouter = require('./sub_categories_r');
var projectRouter = require('./project_r');

//~ Export Router Initializater ~//
module.exports = function initRouter(app) {

    // items Endpoint
    app.use('/items', itemsRouter);

    // Customers Endpoint
    app.use('/customers', customerRouter);

    // Categories Endpoint
    app.use('/categories', categoryRouter);

    // Item prices Endpoint
    app.use('/item_Price', itemPriceRouter);

    // Proformas End Point
    app.use('/proforma', proformaRouter);

    //Proforma Items EndPoint
    app.use('/proforma_Items', proformaItemsRouter);

    //Sales Item EndPoint
    app.use('/sale_Items', saleItemRouter);

    //Sales EndPoint
    app.use('/sales', salesRouter);

    //Stock Items EndPoint
    app.use('/stock_Items', stockItemsRouter);

    //Stock Endpoint
    app.use('/stocks', stockRouter);

    //Sub Categories Endpoint
    app.use('/sub_Categories', subCategoriesRouter);

    //Projects Endpoint
    app.use('/project', projectRouter);
};
