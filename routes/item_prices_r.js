// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();

var items_c = require('../controllers/item_prices_c');

// ~ get all Items ~ //
router.get('/', items_c.getAllItem_Prices);

// ~ get item price with query ~ //
router.post('/find', items_c.getItem_Price);

// ~ register new item ~ //
router.post('/', items_c.addItem_Price);

// ~ update an item ~ //
router.put('/update', items_c.updateItem_Price);

// ~ delete an item
router.delete('/delete', items_c.deleteItem_Price);

// ~ Exports Router ~ //
module.exports = router;