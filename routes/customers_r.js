// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();

var customers_c = require('../controllers/customers_c');

// ~ get all Customers ~ //
router.get('/', customers_c.getAllCustomers);

// ~ get Customer with query ~ //
router.post('/find', customers_c.getCustomer);

// ~ register new Customers ~ //
router.post('/', customers_c.addCustomer);

// ~ update Customer~ //
router.put('/update', customers_c.updateCustomer);

// ~ delete Customer
router.delete('/:id', customers_c.deleteCustomer);

// ~ Exports Router ~ //
module.exports = router;