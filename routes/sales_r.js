// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();

var sales_c = require('../controllers/sales_c');

// ~ get all Items ~ //
router.get('/', sales_c.getAllSales);

// ~ search proforma table for a string ~ //
router.get('/saleWithKeyword', sales_c.searchSales);

// ~ delete request from the search result ~ //
router.delete('/saleWithKeyword/:id', sales_c.deleteSales);

// ~ get item with query ~ //
router.post('/find', sales_c.getSales);

// ~ monthlyReportSales ~ //
router.get('/getTodaysSale', sales_c.getTodaySale);

// ~ monthlyReportSales ~ //
router.get('/monthlyReportSales', sales_c.getMonthlyReportSales);

// ~ weeklyReportSales ~ //
router.get('/weeklyReportSales', sales_c.getWeeklyReportSales);

// ~ query proforma with date range ~ //
router.post('/findByDateRange', sales_c.getSaleWithDateRange);

// ~ dailyReportSales ~ //
router.get('/dailyReportSales', sales_c.getDailyReportSales);

// ~ Find Monthly Sold Items Report ~ //
router.get('/findMonthlySoldItemsReport', sales_c.findMonthlySoldItemsReport);

// ~ getcurrentFSNO ~ //
router.get('/getcurrentFSNO', sales_c.getcurrentFSNO);

// ~ register new item ~ //
router.post('/', sales_c.addSales);

// ~ update an item ~ //
router.put('/update', sales_c.updateSales);

// ~ delete an item
router.delete('/:id', sales_c.deleteSales);

// ~ Exports Router ~ //
module.exports = router;