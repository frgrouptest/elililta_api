// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();

var sub_categories_c = require('../controllers/sub_categories_c');

// ~ get all Items ~ //
router.get('/', sub_categories_c.getAllSub_categories);

// ~ get item with query ~ //
router.get('/find', sub_categories_c.getSub_category);

// ~ delete item with query ~ //
router.delete('/find/:id', sub_categories_c.deleteSub_category);

// ~ register new item ~ //
router.post('/', sub_categories_c.addSub_category);

// ~ update an item ~ //
router.put('/update', sub_categories_c.updateSub_category);

// ~ delete an item
router.delete('/:id', sub_categories_c.deleteSub_category);

// ~ Exports Router ~ //
module.exports = router;