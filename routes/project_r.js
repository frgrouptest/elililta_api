// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();

var projects_c = require('../controllers/projects_c');

// ~ get all projects ~ //
router.get('/', projects_c.getAllProjects);

// ~ get project with query ~ //
router.post('/find', projects_c.getProject);

// ~ get project with query ~ //
router.post('/findItemsOfProject', projects_c.getProjectAllocatedItems);

// ~ register new project ~ //
router.post('/', projects_c.addProject);

// ~ update an project ~ //
router.put('/update', projects_c.updateProject);

// ~ update project allocated items ~ //
router.post('/updateAllocatedItems', projects_c.updateProjectAllocatedItems);

// ~ delete an project
router.delete('/:id', projects_c.deleteProject);

// ~ Exports Router ~ //
module.exports = router;