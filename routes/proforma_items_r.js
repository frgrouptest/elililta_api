// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();

var proforma_item_c = require('../controllers/proforma_item_c');

// ~ get all Customers ~ //
router.get('/all', proforma_item_c.getAllProforma_items);

// ~ get Customer with query ~ //
router.post('/', proforma_item_c.getProforma_item);

// ~ register new Customers ~ //
router.post('/add', proforma_item_c.addProforma_item);

// ~ update Customer~ //
router.put('/update', proforma_item_c.updateProforma_item);

// ~ delete Customer
router.delete('/delete', proforma_item_c.deleteProforma_item);

// ~ Exports Router ~ //
module.exports = router;