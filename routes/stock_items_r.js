// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();

var stock_items_c = require('../controllers/stock_items_c');

// ~ get all Items ~ //
router.get('/', stock_items_c.getAllStock_items);

// ~ get item with query ~ //
router.post('/find', stock_items_c.getStock_item);

// ~ get stock report category wise ~ //
router.get('/byCategory', stock_items_c.getStockItemsByCategory);

// ~ get stock report by product wise ~ //
router.get('/byProduct', stock_items_c.getStockItemsByProduct);

// checkItemQuantityAvailability
router.post('/checkItemQuantityAvailability', stock_items_c.checkItemAvailability);

// Item sold out
router.post('/saveSale', stock_items_c.saveSale);

// ~ add items to stock ~ //
router.post('/', stock_items_c.addStock_item);

// ~ get stockIn history  ~ //
router.get('/stockIns', stock_items_c.getAllStockInHistory);

// ~ update an item ~ //
router.put('/update', stock_items_c.updateStock_item);

// ~ delete an item
router.delete('/:id', stock_items_c.deleteStock_item);

// ~ Exports Router ~ //
module.exports = router;