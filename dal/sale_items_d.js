// ~ Load Module Dependencies ~ //

var models = require('../models');

var Sale_itemModel = models.sale_items;


exports.getAll = function getAll(query, cb) {

  Sale_itemModel.findAll().then(function (sale_items) {
        cb(null,sale_items);
    }).catch( function (err) {
        cb(err,null);
    });

};

exports.registerSale_item = function registerSale_item(Sale_itemData, cb) {

    Sale_itemModel.create(Sale_itemData).then(function (Sale_item) {
       cb(null,Sale_item);
    }).catch( function (err) {
       cb(err,null);
    });

};

exports.deleteSale_item = function deleteSale_item(query, cb) {
    Sale_itemModel.destroy({
        where: query
    }).then(function (no_rows_affected) {
        cb(null,no_rows_affected);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findSale_item = function findSale_item(query, cb) {
    Sale_itemModel.findAll({
        where: query
    }).then(function (Sale_item) {
        cb(null,Sale_item);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.updateSale_item = function updateSale_item(id, updates, cb) {
    Sale_itemModel.update(updates,{where: {'id':id}}).then( function (rows_updated) {
        cb(null,rows_updated);
    }).catch(function (err) {
        cb(err,null);
    });
};