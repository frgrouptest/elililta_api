// ~ Load Module Dependencies ~ //

var models = require('../models');

var SaleModel = models.sale;


exports.getAll = function getAll(_offset, _limit, cb) {

  SaleModel.findAll({   order: [
      ['id', 'DESC']
  ] ,  offset: _offset, limit: _limit }).then(function (Sales) {
        cb(null,Sales);
    }).catch( function (err) {
        cb(err,null);
    });

};

exports.registerSale = function registerSale(SaleData, cb) {

    SaleModel.create(SaleData).then(function (Sale) {
       cb(null,Sale);
    }).catch( function (err) {
       cb(err,null);
    });

};

exports.getWithKeyWord = function getAll(keyword, cb) {

    models.sequelize.query(
        "SELECT * FROM sales WHERE CONCAT_WS ( '' , bill_to, '', TIN_NO, '', address, '', sale_date ) LIKE \"%" + keyword + "%\"",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
        cb(null, res);
}).catch (function (err) {
        cb(err,null);
    });

};

exports.deleteSale = function deleteSale(query, cb) {
    SaleModel.destroy({
        where: query
    }).then(function (no_rows_affected) {
        cb(null,no_rows_affected);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findSale = function findSale(query, cb) {
    SaleModel.findAll({
        where: query
    }).then(function (Sale) {
        cb(null,Sale);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.getTodaysSale = function findMonthlyReportSales(query, cb) {
    models.sequelize.query(
        "SELECT today_sale AS value FROM todays_sale_view ",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
        cb(null, res);
}).catch (function (err) {
        cb(err,null);
    });
};

exports.findMonthlyReportSales = function findMonthlyReportSales(limit, offset, cb) {
    models.sequelize.query(
        "SELECT * FROM (SELECT MONTHNAME(sales.sale_date) as month, SUM(sales.amount) AS sale_value FROM sales\n" +
        "GROUP BY month ) as tmp_table ORDER BY month ASC limit " + limit +" OFFSET " + offset + " ;",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
            cb(null, res);
        }).catch (function (err) {
                cb(err,null);
            });
};

exports.findWeeklyReportSales = function findWeeklyReportSales(limit, offset, cb) {
    models.sequelize.query(
        "SELECT * from (SELECT YEARWEEK( sales.sale_date) AS week, SUM(sales.amount) AS sale_value FROM sales\n" +
        "GROUP BY week) as tmp_table ORDER BY week DESC limit " + limit +" OFFSET " + offset + " ;",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
                cb(null, res);
        }).catch (function (err) {
                cb(err,null);
            });
};

exports.findDailyReportSales = function findDailyReportSales(limit, offset , cb) {

    models.sequelize.query(
        " SELECT * from ( SELECT DATE(sales.sale_date) AS date , SUM(sales.amount) AS sale_value FROM \n" +
        "sales GROUP BY date ) as tmp_table\n" +
        "ORDER BY date DESC limit " + limit +" OFFSET " + offset + " ;",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
                cb(null, res);
        }).catch (function (err) {
                cb(err,null);
            });
};

exports.findMonthlySoldItemsReport = function findMonthlySoldItemsReport(month_number, year, cb) {

    models.sequelize.query(
        "SELECT YEAR(sales.sale_date) as year, MONTHNAME(sales.sale_date) as month , CONCAT_WS ( '' , items.name, \" \", items.subcategory) as item_name , SUM(sale_items.quantity) as quantity FROM sale_items \n" +
        "INNER JOIN items on (items.id = sale_items.itemId)\n" +
        "INNER JOIN sales on (sales.id = sale_items.sales_ID)\n" +
        "WHERE MONTH(sales.sale_date) = " + month_number +  " AND YEAR(sales.sale_date) = " + year + " \n" +
        "GROUP BY year, month, item_name",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
                cb(null, res);
        }).catch (function (err) {
                cb(err,null);
            });
};

exports.findSaleWithDateRange = function findProforma(query, cb) {

    var startDate = query.start;
    var endDate = query.end;

    SaleModel.findAll({
        where: {
            sale_date: {
                $between: [startDate, endDate]
            }
        }
    }).then(function (Sale) {
        cb(null,Sale);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.updateSale = function updateSale(id, updates, cb) {
    SaleModel.update(updates,{where: {'id':id}}).then( function (rows_updated) {
        cb(null,rows_updated);
    }).catch(function (err) {
        cb(err,null);
    });
};



exports.getcurrentFSNO = function getcurrentFSNO( body , cb) {

    models.sequelize.query(
        "SELECT MAX(id) AS currentLastFSNO FROM sales;",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
        cb(null, res);
}).catch (function (err) {
        cb(err,null);
    });
};