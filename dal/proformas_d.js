// ~ Load Module Dependencies ~ //

var models = require('../models');

var ProformaModel = models.proforma;
var Proforma_itemModel = models.proforma_items;
var sequelize = models.sequelize;

exports.getAll = function getAll(_offset,_limit, cb) {

  ProformaModel.findAll({ order: [
      ['id', 'DESC']
  ] , offset: _offset, limit: _limit }).then(function (Proformas) {
        cb(null,Proformas);
    }).catch( function (err) {
        cb(err,null);
    });

};

exports.getWithKeyWord = function getAll(keyword, cb) {

    models.sequelize.query(
        "SELECT * FROM proformas WHERE CONCAT_WS ( '' , bill_to, '', TIN_NO, '', address, '', proforma_date ) LIKE \"%" + keyword + "%\"",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
        cb(null, res);
    }).catch (function (err) {
            cb(err,null);
    });

};

exports.registerProforma = function registerProforma(ProformaData, cb) {

    ProformaModel.create(ProformaData).then(function (Proforma) {
       cb(null,Proforma);
    }).catch( function (err) {
       cb(err,null);
    });

};

exports.registerNewProforma = function registerProforma(ProformaData, cb) {
    var createBillItems = [];
    return sequelize.transaction(function (t) {

        // chain all your queries here. make sure you return them.
        return ProformaModel.create(ProformaData.proformaDetail,
            {transaction: t}).then(function (proforma) {

            for(var i = 0; i < ProformaData.proformaBillItems.length; i++){
                createBillItems.push({
                    proforma_ID: proforma.id,
                    itemId: ProformaData.proformaBillItems[i].itemId,
                    quantity: ProformaData.proformaBillItems[i].quantity,
                    item_price: ProformaData.proformaBillItems[i].item_price,
                    unit: ProformaData.proformaBillItems[i].unit
                });
            }
            return Proforma_itemModel.bulkCreate(
                createBillItems, {transaction: t} );
        });

    }).then(function (result) {
        // Transaction has been committed
        cb(null,result);
        console.log('transaction commited');
    }).catch(function (err) {
        // Transaction has been rolled back
        cb(err,null);
        console.log('Transaction rolled back');
    });

};

exports.deleteProforma = function deleteProforma(query, cb) {
    ProformaModel.destroy({
        where: query
    }).then(function (no_rows_affected) {
        cb(null,no_rows_affected);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findProformas = function findProforma(query, cb) {
    ProformaModel.findAll({
        where: query
    }).then(function (Proforma) {
        cb(null,Proforma);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.findProformaWithDateRange = function findProforma(query, cb) {

    var startDate = query.start;
    var endDate = query.end;

    console.log(endDate);

    ProformaModel.findAll({
        where: {
            proforma_date: {
                $between: [startDate, endDate]
            }
        }
    }).then(function (Proforma) {
        cb(null,Proforma);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.updateProforma = function updateProforma(id, updates, cb) {
    ProformaModel.update(updates,{where: {'id':id}}).then( function (rows_updated) {
        cb(null,rows_updated);
    }).catch(function (err) {
        cb(err,null);
    });
};