// ~ Load Module Dependencies ~ //

var models = require('../models');

var itemModel = models.items;
var sequelize = models.sequelize;

var Item_PriceModel = models.item_price;

exports.getAll = function getAll(query, cb) {

  itemModel.findAll({
      where: {deleted: false}
  }).then(function (items) {
        cb(null,items);
    }).catch( function (err) {
        console.log(err);
        cb(err,null);
    });

};

exports.registerItem = function registerItem(itemData, cb) {

    var createItemPrices = [];
    return sequelize.transaction(function (t) {

        // chain all your queries here. make sure you return them.
        return itemModel.create(itemData,
            {transaction: t}).then(function (item) {

            for(var i = 0; i < itemData.item_prices.length; i++){
                createItemPrices.push({
                    item_id: item.id,
                    name: 'Price' + (i+1),
                    value: itemData.item_prices[i]
                });
            }
            return Item_PriceModel.bulkCreate(
                createItemPrices, {transaction: t} );
        });

    }).then(function (result) {
        // Transaction has been committed
        cb(null,result);
        console.log('transaction commited');
    }).catch(function (err) {
        // Transaction has been rolled back
        cb(err,null);
        console.log('Transaction rolled back');
    });

};

exports.deleteItem = function deleteItem(query, cb) {
    itemModel.update({deleted : true},{
        where: query
    }).then(function (no_rows_affected) {
        cb(null,no_rows_affected);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findItems = function findItem(query, cb) {
    query['deleted'] = false;
    itemModel.findAll({
        where: query
    }).then(function (item) {
        cb(null,item);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findItemsLike = function findItem(query, cb) {
    var _val_ = query.value;
    var _category_ = query.category;
    var _sub_category_ = query.subCategory;
    var _col_ = query.col;

    var _query_ = {};
    _query_['deleted'] = false;
    _query_[_col_] = { $like: '%' + _val_ + '%' };
    if( (_category_) && (_category_.length > 1) ){
        _query_['category'] = _category_;
    }
    if( (_sub_category_) && (_sub_category_.length > 1) ){
        _query_['subcategory'] = _sub_category_;
    }


    itemModel.findAll({
        where: _query_
    }).then(function (item) {
        cb(null,item);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.updateItem = function updateItem(id, updates, cb) {

    var createItemPrices = [];
    return sequelize.transaction(function (t) {

        // chain all your queries here. make sure you return them.
        return itemModel.update(updates,{where: {'id':id}},
            {transaction: t}).then(function (item) {

            for(var i = 0; i < updates.item_prices.length; i++){
                createItemPrices.push({
                    item_id: id,
                    name: 'Price' + (i+1),
                    value: updates.item_prices[i]
                });
            }

            return Item_PriceModel.destroy({where : {item_id : id}, transaction : t}).then(function (res) {
                return Item_PriceModel.bulkCreate(
                    createItemPrices, {transaction: t} );
            });

        });

    }).then(function (result) {
        // Transaction has been committed
        cb(null,result);
        console.log('transaction commited');
    }).catch(function (err) {
        // Transaction has been rolled back
        cb(err,null);
        console.log('Transaction rolled back');
    });

};