'use strict';
module.exports = (sequelize, DataTypes) => {
  var sub_category = sequelize.define('sub_category', {
    categoryId: DataTypes.INTEGER,
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return sub_category;
};