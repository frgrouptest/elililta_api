'use strict';
module.exports = (sequelize, DataTypes) => {
  var proforma = sequelize.define('proforma', {
    bill_to: DataTypes.STRING,
    TIN_NO: DataTypes.STRING,
    address: DataTypes.STRING,
    proforma_date: DataTypes.DATE,
    reference: DataTypes.STRING,
    FSNO: DataTypes.STRING,
    discount: DataTypes.FLOAT,
    amount: DataTypes.FLOAT,
    deleted : DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return proforma;
};