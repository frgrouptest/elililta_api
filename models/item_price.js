'use strict';
module.exports = (sequelize, DataTypes) => {
  var item_price = sequelize.define('item_price', {
    item_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    value: DataTypes.FLOAT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return item_price;
};