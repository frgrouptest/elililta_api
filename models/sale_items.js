'use strict';
module.exports = (sequelize, DataTypes) => {
  var sale_items = sequelize.define('sale_items', {
    sales_ID: DataTypes.INTEGER,
    itemId: DataTypes.INTEGER,
    quantity: DataTypes.INTEGER,
    item_price: DataTypes.FLOAT,
    unit: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return sale_items;
};