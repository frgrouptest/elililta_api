'use strict';
module.exports = (sequelize, DataTypes) => {
  var items = sequelize.define('items', {
    custom_id: DataTypes.STRING,
    name: DataTypes.STRING,
    color: DataTypes.STRING,
    unit: DataTypes.STRING,
    kg_per_bar : DataTypes.INTEGER,
    category: DataTypes.STRING,
    stock_price: DataTypes.INTEGER,
    subcategory: DataTypes.STRING,
    item_description: DataTypes.STRING,
    stock_reorder_level: DataTypes.STRING,
    deleted : DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          items.hasMany(models.item_price,
              {foreignKey: 'item_Id'});
      }
    }
  });
  return items ;
};