'use strict';
module.exports = (sequelize, DataTypes) => {
  var stock_ins = sequelize.define('stock_ins', {
    itemId: DataTypes.INTEGER,
    quantity: DataTypes.INTEGER,
    date_in: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return stock_ins;
};