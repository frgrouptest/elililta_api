'use strict';
module.exports = (sequelize, DataTypes) => {
  var Project = sequelize.define('project', {
    name: DataTypes.STRING,
    amount: DataTypes.BIGINT,
    address: DataTypes.STRING,
    client: DataTypes.STRING,
    agreement: DataTypes.TEXT,
    deleted : DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Project;
};