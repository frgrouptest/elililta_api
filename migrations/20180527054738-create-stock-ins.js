'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('stock_ins', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      itemId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
              model: "items",
              key: "id"
          },
          onDelete: 'CASCADE'
      },
      quantity: {
        type: Sequelize.INTEGER
      },
      date_in: {
        type: Sequelize.DATEONLY
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('stock_ins');
  }
};