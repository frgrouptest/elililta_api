'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
    // logic for transforming into the new state
    return queryInterface.sequelize.query('CREATE VIEW ims_elilta.todays_sale_view AS SELECT SUM(sales.amount) as today_sale, sales.sale_date\n' +
        'FROM sales \n' +
        'WHERE DATE(sales.sale_date) = CURDATE()\n' +
        'GROUP BY sales.sale_date');
},

    down: (queryInterface, Sequelize) => {
    // logic for reverting the changes
    return queryInterface.sequelize.query('DROP VIEW ims_elilta.todays_sale_view;');
  }
}