'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('proforma_items', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      proforma_ID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: "proformas",
            key: "id"
        },
        onDelete: 'CASCADE'
      },
      itemId: {
        type: Sequelize.INTEGER,
        references: {
            model: "items",
            key: "id"
        }
      },
      quantity: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      item_price: {
        type: Sequelize.FLOAT
      },
      unit: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('proforma_items');
  }
};