'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
    // logic for transforming into the new state
    return queryInterface.sequelize.query('CREATE VIEW ims_elilta.item_scarce_in_stock_view AS SELECT items.name\n' +
        'FROM stock_items, items\n' +
        'WHERE (stock_items.quantity <= stock_items.reoder_level) \n' +
        '\tAND (items.id = stock_items.itemId)\n' +
        '\tAND (items.deleted = false) ');
},

down: (queryInterface, Sequelize) => {
    // logic for reverting the changes
    return queryInterface.sequelize.query('DROP VIEW ims_elilta.item_scarce_in_stock_view;');
    }
}