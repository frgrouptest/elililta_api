'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('sales', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      bill_to: {
        type: Sequelize.STRING,
        allowNull: false
      },
      TIN_NO: {
        type: Sequelize.STRING
      },
      address: {
        type: Sequelize.STRING
      },
      sale_date: {
        type: Sequelize.DATEONLY
      },
      reference: {
        type: Sequelize.STRING
      },
      FSNO: {
        type: Sequelize.STRING
      },
      discount: {
        type: Sequelize.FLOAT
      },
      amount: {
          type: Sequelize.FLOAT,
          allowNull: false
      },
      withVat: {
          type: Sequelize.BOOLEAN,
          default : true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deleted: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: 0
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('sales');
  }
};