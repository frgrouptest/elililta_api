var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var config = require('./config/')
var validator    = require('express-validator');
var router = require('./routes');

// ~ Cross origin resource sharing ~ //
var cors = require('cors');

// ~ Initialize the app ~ //
var app = express();

// ~ Enbling CORS ~ //
app.use(cors());

//~ Set Middleware ~//
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());


//~ Set Validator ~//
app.use(validator());

//~ Set Routes ~//
router(app);


//~ Listen to HTTP Port ~//
app.listen(config.HTTP_PORT,function connectionListener() {
    console.log(' API Server is running on port %s',config.HTTP_PORT);
});
