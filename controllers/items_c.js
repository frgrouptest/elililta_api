// ~ Load Module Dependecies ~ //

var events = require('events');
var itemsDal = require('../dal/items_d');


exports.getAllItems = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        itemsDal.getAll({}, function callback(err, items) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(items);
        });
    });

    workflow.emit('performRequest');

};

exports.addItem = function (req, res, next) {
    var workflow = new events.EventEmitter();
    console.log(req.body);
    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'Name is empty').notEmpty();
        req.checkBody('unit', 'Unit is empty').notEmpty();
        req.checkBody('category', 'category is empty').notEmpty();
        req.checkBody('stock_price', 'Stock price is empty').notEmpty();
        req.checkBody('stock_reorder_level', 'Stock reorder level is empty').notEmpty();


        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
            next();
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        itemsDal.registerItem(req.body, function callback(err, item) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(item);
        });

    });

    workflow.emit('validateRequest');

};

exports.deleteItem = function (req, res, next) {

    var workflow = new events.EventEmitter();
    var _query_ = { id : req.params.id};
    workflow.on('performRequest', function performRequest() {
        itemsDal.deleteItem(_query_ , function callback(err, no_rows_affected) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.send({'items_deleted': no_rows_affected});
        });
    });

    workflow.emit('performRequest');
};

exports.updateItem = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var itemId = req.params.id;
    var body  = req.body;
console.log(body);
// console.log(req);
    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'name is empty').notEmpty();
        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {
        itemsDal.updateItem(itemId, body, function callback(err, rows_updated) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;

            }

            res.status(201);
            res.json({'Items updated ': rows_updated});
        });
    });

    workflow.emit('validateRequest');
};

exports.getItem = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        itemsDal.findItems(req.body, function callback(err, items) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(items);
        });

    });

    workflow.emit('performRequest');

};


exports.getItemLike = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        itemsDal.findItemsLike(req.body, function callback(err, items) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(items);
        });

    });

    workflow.emit('performRequest');

};