// ~ Load Module Dependecies ~ //

var events = require('events');
var ProformasDal = require('../dal/proformas_d');


exports.getAllProformas = function (req, res, next) {
    var workflow = new events.EventEmitter();

    var offset = parseInt(req.query.offset) || 0;
    var limit  = parseInt(req.query.limit) || 3;

    workflow.on('performRequest', function performRequest() {
        ProformasDal.getAll(offset,limit, function callback(err, Proformas) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Proformas);
        });
    });

    workflow.emit('performRequest');

};

exports.searchProforma = function (req, res, next) {
    var workflow = new events.EventEmitter();

    var keyword = req.query.keyword;

    workflow.on('performRequest', function performRequest() {
        ProformasDal.getWithKeyWord(keyword , function callback(err, Proformas) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Proformas);
        });
    });

    workflow.emit('performRequest');

};

exports.addProforma = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('validateRequest', function validateRequest() {

        // Validations
        req.checkBody('bill_to', 'bill to cannot be empty').notEmpty();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        ProformasDal.registerProforma(req.body, function callback(err, Proforma) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Proforma);
        });

    });

    workflow.emit('validateRequest');

};

exports.addNewProforma = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('validateRequest', function validateRequest() {

        // Validations
        req.checkBody('proformaDetail', 'Proforma detail is empty.').notEmpty();
        req.checkBody('proformaBillItems', 'Can not register proforma with empty registered items.').notEmpty();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        ProformasDal.registerNewProforma(req.body, function callback(err, Proforma) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Proforma);
            next();
        });

    });

    workflow.emit('validateRequest');

};


exports.deleteProforma = function (req, res, next) {

    var workflow = new events.EventEmitter();
    var _query_ = { id : req.params.id};
    workflow.on('performRequest', function performRequest() {
        ProformasDal.deleteProforma(_query_ , function callback(err, no_rows_affected) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.send({'Proformas_deleted': no_rows_affected});
        });
    });

    workflow.emit('performRequest');
};

exports.updateProforma = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var ProformaId = req.body.id;
    var body  = req.body;

console.log(req.body);
    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'name is empty').notEmpty();
        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {
        ProformasDal.updateProforma(ProformaId, body, function callback(err, rows_updated) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;

            }

            res.status(201);
            res.json({'Proformas updated ': rows_updated});
        });
    });

    workflow.emit('validateRequest');
};

exports.getProforma = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        ProformasDal.findProformas(req.body, function callback(err, Proformas) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Proformas);
        });

    });

    workflow.emit('performRequest');

};



exports.getProformaWithDateRange = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        ProformasDal.findProformaWithDateRange(req.body, function callback(err, Proformas) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Proformas);
        });

    });

    workflow.emit('performRequest');

};
