// ~ Load Module Dependecies ~ //

var events = require('events');
var StocksDal = require('../dal/stock_d');


exports.getStockDetail = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        StocksDal.getStockInformation(function callback(err, Stocks) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Stocks);
        });
    });

    workflow.emit('performRequest');

};

exports.getItemsLowInStock = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        StocksDal.getItemsLowInStock_(function callback(err, Stocks) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Stocks);
        });
    });

    workflow.emit('performRequest');

};