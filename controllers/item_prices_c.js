// ~ Load Module Dependecies ~ //

var events = require('events');
var Item_PricesDal = require('../dal/item_prices_d');


exports.getAllItem_Prices = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        Item_PricesDal.getAll({}, function callback(err, Item_Prices) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Item_Prices);
        });
    });

    workflow.emit('performRequest');

};

exports.addItem_Price = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'name is empty').notEmpty();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        Item_PricesDal.registerItem_Price(req.body, function callback(err, Item_Price) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Item_Price);
        });

    });

    workflow.emit('validateRequest');

};

exports.deleteItem_Price = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        Item_PricesDal.deleteItem_Price(req.body , function callback(err, no_rows_affected) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.send({'Item_Prices_deleted': no_rows_affected});
        });
    });

    workflow.emit('performRequest');
};

exports.updateItem_Price = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var Item_PriceId = req.body.id;
    var body  = req.body;

console.log(req.body);
    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'name is empty').notEmpty();
        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {
        Item_PricesDal.updateItem_Price(Item_PriceId, body, function callback(err, rows_updated) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json({'Item_Prices updated ': rows_updated});
        });
    });

    workflow.emit('validateRequest');
};

exports.getItem_Price = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        Item_PricesDal.findItem_Prices(req.body, function callback(err, Item_Prices) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Item_Prices);
        });

    });

    workflow.emit('performRequest');

};
