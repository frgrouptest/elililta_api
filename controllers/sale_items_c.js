// ~ Load Module Dependecies ~ //

var events = require('events');
var Sale_itemsDal = require('../dal/sale_items_d');


exports.getAllSale_items = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        Sale_itemsDal.getAll({}, function callback(err, Sale_items) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sale_items);
        });
    });

    workflow.emit('performRequest');

};

exports.addSale_item = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('validateRequest', function validateRequest() {

        req.checkBody('item_price', 'item_price is empty').notEmpty();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        Sale_itemsDal.registerSale_item(req.body, function callback(err, Sale_item) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sale_item);
        });

    });

    workflow.emit('validateRequest');

};

exports.deleteSale_item = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        Sale_itemsDal.deleteSale_item(req.body , function callback(err, no_rows_affected) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.send({'Sale_items_deleted': no_rows_affected});
        });
    });

    workflow.emit('performRequest');
};

exports.updateSale_item = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var Sale_itemId = req.body.id;
    var body  = req.body;

console.log(req.body);
    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('item_price', 'item_price is empty').notEmpty();
        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {
        Sale_itemsDal.updateSale_item(Sale_itemId, body, function callback(err, rows_updated) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json({'Sale_items updated ': rows_updated});
        });
    });

    workflow.emit('validateRequest');
};

exports.getSale_item = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        Sale_itemsDal.findSale_item(req.body, function callback(err, Sale_items) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sale_items);
        });

    });

    workflow.emit('performRequest');

};
